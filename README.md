# Telegram CTF task bot 

## Description

## Deploy
1. Создать `.env` файл, в него положить следующие строчки:
```
BOT_TOKEN=Ваш токен для коннекта к API вашего бота, брать из BotFather
REPO_URL=SSH путь до репозитория с тасками
```
2. Создать директорию _rsa_keys_, в нее необходимо положить файлы:
- _config_ (пример заполнения, поле *IdentityFile* оставить без изменения,
в поле *User* укажите имя пользователя, в *Hosts* - используемый хостинг)
```
# SSH Config to you git repo
Host gitlab.com
User bot_user
IdentityFile ~/.ssh/ctf_id_rsa
StrictHostKeyChecking no
UserKnownHostsFile=/dev/null
```
- _ctf_id_rsa_
```
Приватный ключ доступа к аккаунту gitlab/github класть в него
```
Примечание: для обеспечения безопасности создайте боту отдельный акк, 
положите туда ключ
и выдайте права на чтение репозитория для обеспечения безопасности!  
3. Выполнить команду:
```
docker-compose up --build -d
```

## Environment

1) http://localhost:5050 - веб админ-панель для Postgres
2) На порту *5432* работает сама БД
