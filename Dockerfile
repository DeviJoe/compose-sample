FROM python:3.9

WORKDIR /ctf-csf/ctf-bot/src
ENV PYTHONPATH=/ctf-csf/ctf-bot/
ENV CONFIG_PATH=/ctf-csf/ctf-bot/src/config.yml
ENV STATIC_CONTENT_PATH=/ctf-csf/ctf-bot/src/static/
ENV CATEGORIES=/ctf-csf/ctf-bot/src/category.yml
COPY . /ctf-csf/ctf-bot
COPY ./rsa_keys/config /root/.ssh/config
COPY ./rsa_keys/ctf_id_rsa /root/.ssh/ctf_id_rsa
RUN apt-get install -y --no-install-recommends git
RUN pip install --no-cache-dir -r /ctf-csf/ctf-bot/requirements.txt
RUN chmod 600 ../rsa_keys/ctf_id_rsa

ENTRYPOINT ["python3", "ctf_bot.py"]
