from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.types import ParseMode

from src.models.User import User
from src.services import db_sevice
from src.services.task_service import InternalTask, TaskWorker


async def profile(msg: types.Message, task_worker):
    usr: User = db_sevice.get_user(msg.from_user.id)
    position = 0
    board = db_sevice.scoreboard(task_worker)
    for i, pos in enumerate(board):
        if pos[0] == usr.username:
            position = i + 1
    res = f'''
    ***==========[ PROFILE ]==========***
    ID: {usr.tg_id}
    Ф.И.О: {usr.fio}
    Username: {usr.username}
    VK: {usr.vk_url}

    Решено тасков: {len(db_sevice.select_all_passed_task(usr.tg_id))}
    Счет: {db_sevice.get_user_score(usr.tg_id, task_worker)}
    Позиция в рейтинге: {position}
                '''
    await msg.answer(res, parse_mode=ParseMode.MARKDOWN)


async def send_task(msg: types.Message, task: InternalTask):
    res = f'''
***=========[ {task.name} ]=========***

{task.description}

Score: {task.chart['score']}
'''
    await msg.answer(res, parse_mode=ParseMode.MARKDOWN)
    if len(task.file_paths) > 0:
        for doc in task.file_paths:
            await msg.answer_document(open(doc))


async def send_scoreboard(msg: types.Message, task_manager: TaskWorker):
    res = f'''
***======[ SCOREBOARD ]======***\n
'''
    grades = ['🥇', '🥈', '🥉']
    counter = 0
    board = db_sevice.scoreboard(task_manager)
    for i, position in enumerate(board):
        usr_id = db_sevice.get_tg_by_username(position[0])
        if db_sevice.if_in_admin_list(usr_id):
            counter += 1
            continue
        res += f'{i + 1 - counter}) {position[0]} - [ {position[1]} ]'
        if i - counter <= 2:
            res += grades[i - counter]
        if msg.from_user.id == db_sevice.get_tg_by_username(position[0]):
            res += '◀️'
        res += '\n'
    await msg.answer(res, parse_mode=ParseMode.MARKDOWN)