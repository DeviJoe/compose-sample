import peewee
from src.models.BaseModel import BaseModel
from src.models.User import User
from src.models.Task import Task


class Progress(BaseModel):
    user_id = peewee.ForeignKeyField(model=User, backref='tg_id', column_name='user_id')
    task_name = peewee.ForeignKeyField(model=Task, backref='name', column_name='task_id')
    timestamp = peewee.DoubleField(column_name='timestamp')

    class Meta:
        table_name = 'progress'
        primary_key = peewee.CompositeKey('user_id', 'task_name')