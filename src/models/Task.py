import peewee
from src.models.BaseModel import BaseModel


class Task(BaseModel):
    tg_id = peewee.AutoField(column_name='id', primary_key=True)
    name = peewee.TextField(column_name='name')

    class Meta:
        table_name = 'tasks'
