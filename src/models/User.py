import peewee
from src.models.BaseModel import BaseModel


class User(BaseModel):
    tg_id = peewee.IntegerField(column_name='tg_id', primary_key=True)
    fio = peewee.TextField(column_name='fio')
    username = peewee.TextField(column_name='username')
    vk_url = peewee.TextField(column_name='vk_url')

    class Meta:
        table_name = 'users'
