import peewee
from src.models.BaseModel import BaseModel
from src.models.User import User
from src.models.Task import Task


class Admin(BaseModel):
    tg_id = peewee.ForeignKeyField(model=User, backref='tg_id', column_name='tg_id', primary_key=True)

    class Meta:
        table_name = 'admins'
