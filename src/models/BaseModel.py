import os

import peewee
import yaml


config = yaml.safe_load(open(os.environ['CONFIG_PATH']))
conn = peewee.PostgresqlDatabase(config['db']['db_name'], user=config['db']['login'], password=config['db']['password'],
                                 host=config['db']['host'], port=5432)


class BaseModel(peewee.Model):
    class Meta:
        database = conn
