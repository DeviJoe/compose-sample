import os
import yaml
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher, FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import ParseMode, ContentType
from aiogram.utils import executor
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.contrib.fsm_storage.redis import RedisStorage2
import logging
from services.task_service import TaskWorker
from src.keyboards import reg_kb, main_kb, category_kb, all_task_kb, active_task_kb
from util import file_util
from services import db_sevice
from src import bot_modules

config = yaml.safe_load(open(os.environ.get('CONFIG_PATH')))
logging.basicConfig(level=logging.INFO, filename='/ctf-csf/tasks/ctf-bot.log')
bot = Bot(token=os.environ['BOT_TOKEN'])
states = RedisStorage2(config['redis']['host'], config['redis']['port'])
dp = Dispatcher(bot, storage=states)
task_worker = TaskWorker(os.environ['TASKS_DIR'])
usr_len = int(os.environ.get('USERNAME_LEN'))
cat_list = task_worker.get_category_list()


class PresData(StatesGroup):
    fio = State()
    username = State()
    vk_url = State()


class AllTasks(StatesGroup):
    category = State()
    task = State()


class Active(StatesGroup):
    task = State()


class Message(StatesGroup):
    msg = State()


@dp.message_handler(commands=['start'])
async def start(msg: types.Message, state: FSMContext):
    if db_sevice.get_user(msg.from_user.id) is None:
        await msg.answer(file_util.get_static_content('start'), reply_markup=reg_kb(), parse_mode=ParseMode.MARKDOWN)
    else:
        await state.finish()
        await msg.answer('***С возвращением! Мы скучали***', reply_markup=main_kb(), parse_mode=ParseMode.MARKDOWN)


@dp.message_handler(commands=['update'])
async def update(msg: types.Message):
    if db_sevice.if_in_admin_list(msg.from_user.id):
        task_worker.update()
        await msg.answer('Бот успешно обновлен')
    else:
        logging.info(f'User {db_sevice.get_user(msg.from_user.id).username} try to get ROOT - update')
        await msg.answer('Не хватает полномочий!')


@dp.message_handler(commands=['delete'])
async def delete(msg: types.Message):
    if db_sevice.if_in_admin_list(msg.from_user.id):
        await msg.answer(msg.get_args().split()[0])
        db_sevice.delete_task(msg.get_args().split()[0])
        await msg.answer('Таск удален успешно!')
    else:
        logging.info(f'User {db_sevice.get_user(msg.from_user.id).username} try to get ROOT - update')
        await msg.answer('Не хватает полномочий!')


@dp.message_handler(commands=['echo'])
async def echo(msg: types.Message):
    if db_sevice.if_in_admin_list(msg.from_user.id):
        await msg.answer('Введите сообщение для рассылки или /cancel для отмены', reply_markup=types.ReplyKeyboardRemove())
        await Message.msg.set()
    else:
        logging.info(f'User {db_sevice.get_user(msg.from_user.id).username} try to get ROOT - echo')
        await msg.answer('Не хватает полномочий!')


@dp.message_handler(commands=['cancel'], state=Message.msg)
async def cancel(msg: types.Message, state: FSMContext):
    await state.finish()
    await msg.answer('Рассылка отменена, возврат на главную', reply_markup=main_kb())


@dp.message_handler(state=Message.msg)
async def message_handler(msg: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['msg'] = msg.text
        users_list = db_sevice.get_users_tg_list()
        for usr in users_list:
            await bot.send_message(usr, data['msg'], parse_mode=ParseMode.MARKDOWN)
    await msg.answer('Сообщение успешно разослано!', reply_markup=main_kb())
    await state.finish()


@dp.message_handler(content_types=ContentType.TEXT)
async def handler(msg: types.Message):
    if db_sevice.get_user(msg.from_user.id) is None:
        if msg.text.lower() == 'регистрация':
            await msg.answer('Введите свое ФИО. Просьба указывать корректно!', reply_markup=None)
            await PresData.fio.set()
        else:
            await msg.answer('Вы не зарегистрированы. Отправьте команду /start и следуйте инструкциям')
    else:
        if msg.text.lower() == 'профиль':
            await bot_modules.profile(msg, task_worker)
        elif msg.text.lower() == 'сдать флаг':
            await msg.answer(file_util.get_static_content('flag'), parse_mode=ParseMode.MARKDOWN)
        elif msg.text.lower() == 'все задачи':
            tasks = db_sevice.select_unpassed_tasks(msg.from_user.id, task_worker)
            await msg.answer('Выберите категорию, которую хотите посмотреть', reply_markup=category_kb(cat_list))
            await AllTasks.category.set()
        elif msg.text.lower() == 'активное':
            tasks = db_sevice.select_unpassed_tasks(msg.from_user.id, task_worker)
            await msg.answer('Выберите таск, который хотите посмотреть', reply_markup=active_task_kb(tasks))
            await Active.task.set()
        elif msg.text.lower() == 'таблица лидеров':
            await bot_modules.send_scoreboard(msg, task_worker)
        else:
            flag = msg.text
            task_name = task_worker.get_task_by_flag(flag)
            if task_name is None:
                await msg.answer("Флаг не существует")
                return
            else:
                ch = db_sevice.is_flag_passed(msg.from_user.id, flag, task_worker)
                if ch:
                    await msg.answer('Этот флаг уже был сдан')
                else:
                    db_sevice.add_progress(msg.from_user.id, task_name)
                    await msg.answer("Флаг принят!")
                    logging.info(f'User {db_sevice.get_user(msg.from_user.id).username} passed '
                                 f'flag from task - {task_name}')


@dp.message_handler(state=Active.task)
async def get_active_task(msg: types.Message, state: FSMContext):
    if msg.text.lower() == 'назад':
        await msg.answer('Возврат в главное меню', reply_markup=main_kb())
        await state.finish()
    else:
        async with state.proxy() as data:
            task_name = msg.text
            tasks = db_sevice.select_unpassed_tasks(msg.from_user.id, task_worker)
            for t in tasks:
                if tasks[t].name == task_name:
                    await bot_modules.send_task(msg, tasks[t])


@dp.message_handler(state=AllTasks.category)
async def get_category(msg: types.Message, state: FSMContext):
    if msg.text.lower() == 'назад':
        await msg.answer('Возврат в главное меню', reply_markup=main_kb())
        await state.finish()
    else:
        if msg.text in cat_list:
            async with state.proxy() as data:
                data['category'] = msg.text.lower()
                tasks = task_worker.get_tasks_by_category(data['category'])
                if len(tasks) == 0:
                    await msg.answer('Тасков в категории не найдено')
                    return
            await msg.answer('Выберите задачу', reply_markup=all_task_kb(task_worker.get_tasks_by_category(msg.text)))
            await AllTasks.task.set()


@dp.message_handler(state=AllTasks.task)
async def get_task(msg: types.Message, state: FSMContext):
    if msg.text.lower() == 'назад':
        await msg.answer('Возврат на предыдущую страницу', reply_markup=category_kb(cat_list))
        await AllTasks.category.set()
    else:
        async with state.proxy() as data:
            task_name = msg.text
            tasks = task_worker.get_tasks_by_category(data['category'])
            for task in tasks:
                if task.name == task_name:
                    await bot_modules.send_task(msg, task)


@dp.message_handler(state=PresData.fio)
async def fio(msg: types.Message, state: FSMContext):
    async with state.proxy() as data:
        if len(msg.text.lower().split()) == 3:
            data['fio'] = msg.text
            await msg.answer(
                'Введите свой никнейм, он будет отображаться в таблице лидеров. Придумайте что-то крутое. '
                f'Длина - {usr_len} символов :)')
            await PresData.username.set()
        else:
            await msg.answer('Что-то пошло не так, введенные данные не корректы, повторите ввод')
            return


@dp.message_handler(state=PresData.username)
async def username(msg: types.Message, state: FSMContext):
    async with state.proxy() as data:
        if len(msg.text.lower()) <= 20:
            data['username'] = msg.text.lower()
            await msg.answer(
                'Введите свою ссылку на страницу ВК. Подставлять вместе с https://vk.com')
            await PresData.vk_url.set()
        else:
            await msg.answer('Что-то пошло не так, введенные данные не корректны, повторите ввод')
            return


@dp.message_handler(state=PresData.vk_url)
async def username(msg: types.Message, state: FSMContext):
    async with state.proxy() as data:
        if 'https://vk.com/' in msg.text.lower():
            data['vk_url'] = msg.text.lower()
            await state.finish()
            db_sevice.add_user(msg.from_user.id, data['fio'], data['username'], data['vk_url'])
            await msg.answer('Поздравляем! Вы успешно авторизовались! Удачи! Да пребудет с вами Крис Касперски!',
                             reply_markup=main_kb())
        else:
            await msg.answer('Что-то пошло не так, введенные данные не корректны, повторите ввод')
            return

if __name__ == '__main__':
    executor.start_polling(dp)

