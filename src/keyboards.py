import logging
import typing

from aiogram.types import ReplyKeyboardRemove, ReplyKeyboardMarkup, KeyboardButton

from src.services.task_service import InternalTask


def main_kb():
    btn_task = KeyboardButton('Сдать флаг')
    btn_profile = KeyboardButton('Профиль')
    btn_all_task = KeyboardButton('Все задачи')
    btn_active = KeyboardButton('Активное')
    btn_scoreboard = KeyboardButton('Таблица лидеров')
    kb = ReplyKeyboardMarkup(resize_keyboard=True)
    kb.row(btn_profile, btn_task)
    kb.row(btn_all_task, btn_active)
    kb.add(btn_scoreboard)
    return kb


def reg_kb():
    reg_btn = KeyboardButton('Регистрация')
    kb = ReplyKeyboardMarkup(resize_keyboard=True)
    kb.add(reg_btn)
    return kb


def category_kb(cat_list: list):
    kb = ReplyKeyboardMarkup(resize_keyboard=True)
    for elem in cat_list:
        kb.add(KeyboardButton(elem))
    btn_cancel = KeyboardButton('Назад')
    kb.add(btn_cancel)
    return kb


def all_task_kb(tasks):
    kb = ReplyKeyboardMarkup(resize_keyboard=True)
    for elem in tasks:
        kb.add(KeyboardButton(elem.name))
    btn_cancel = KeyboardButton('Назад')
    kb.add(btn_cancel)
    return kb
    pass


def active_task_kb(tasks):
    kb = ReplyKeyboardMarkup(resize_keyboard=True)
    for elem in tasks:
        kb.add(KeyboardButton(tasks[elem].name))
    btn_cancel = KeyboardButton('Назад')
    kb.add(btn_cancel)
    return kb
    pass
