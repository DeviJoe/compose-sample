import os


def get_static_content(static_file_name):
    f = open(os.environ.get('STATIC_CONTENT_PATH') + static_file_name + '.md', 'r')
    res = f.read()
    return res
