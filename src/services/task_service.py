import dataclasses
import os
import logging
import time
import typing
import yaml
from src.services.db_sevice import *


@dataclasses.dataclass
class InternalTask:
    name: str
    description: str
    chart: dict
    # files: typing.List[typing.IO]
    file_paths: typing.List[str]


class TaskWorker:
    waiting_for_clone_time = 3

    def __init__(self, path) -> None:
        super().__init__()
        # путь до папки, куда клонить репу с тасками
        self.path = path
        # Стянули репу с тасками
        root, dirs, files = os.walk(self.path).__next__()
        if len(dirs) == 0:
            os.system(f'git -C {self.path} clone ' + os.environ.get('REPO_URL'))
            root, dirs, files = os.walk(self.path).__next__()
            self.path += os.sep + dirs[0]
        else:
            root, dirs, files = os.walk(self.path).__next__()
            self.path += os.sep + dirs[0]
            self.__pull()
        time.sleep(self.waiting_for_clone_time)
        # Генерация структуры тасков
        self.tasks: typing.Dict[InternalTask] = {}
        task_list = self.__get_tasks()
        for t in task_list:
            if self.__get_task_by_name(t).chart['enable']:
                self.tasks[t] = self.__get_task_by_name(t)
        # Внесение изменений в БД
        for task in self.tasks:
            add_task(task)

    def __pull(self):
        os.system('git -C ' + self.path + ' pull')

    def __get_tasks(self):
        root, dirs, files = os.walk(self.path).__next__()
        dirs.remove('.git')
        return dirs

    def __get_task_by_name(self, name):
        chart = yaml.safe_load(open(self.path + os.sep + name + os.sep + 'Chart.yml', 'r'))
        description = open(self.path + os.sep + name + os.sep + 'Description.md', 'r').read()

        files_path = self.path + os.sep + name + os.sep + 'files'
        filesIO = []
        fpaths = []
        if os.path.exists(files_path):
            _, _, files = os.walk(files_path).__next__()
            for f in files:
                file = open(files_path + os.sep + f, encoding='utf-8')
                fpaths.append(files_path + os.sep + f)
                # filesIO.append(file)
        return InternalTask(name, description, chart, fpaths)

    def update(self):
        if not os.path.exists(self.path):
            os.system(f'git -C {self.path} clone ' + os.environ.get('REPO_URL'))
        else:
            self.__pull()
        time.sleep(self.waiting_for_clone_time)

        # Генерация структуры тасков
        self.tasks = {}
        task_list = self.__get_tasks()
        for t in task_list:
            if self.__get_task_by_name(t).chart['enable']:
                self.tasks[t] = self.__get_task_by_name(t)

        # Внесение изменений в БД
        for task in self.tasks:
            add_task(task)

    def get_tasks_by_category(self, category: str):
        res = []
        for t in self.tasks:
            if self.tasks[t].chart['category'] == category.lower():
                res.append(self.tasks[t])
        return res

    def get_category_list(self) -> typing.List[InternalTask]:
        res = []
        for t in self.tasks:
            res.append(self.tasks[t].chart['category'])
        return res

    def get_task_by_flag(self, flag):
        for task_name in self.tasks:
            if self.tasks[task_name].chart['flag'] == flag:
                return self.tasks[task_name].name
        return None
