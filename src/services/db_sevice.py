import datetime
import random
import typing
from functools import cmp_to_key
import peewee
from aiogram import types

from src.models.User import User
from src.models.Task import Task
from src.models.Progress import Progress
from src.models import BaseModel
from src.models.Admin import Admin
from src.services.task_service import TaskWorker
from src.services.task_service import InternalTask


def add_user(tg_id, fio, username, vk_url):
    try:
        User.insert(tg_id=tg_id, fio=fio, username=username, vk_url=vk_url).execute()
    except:
        return


def add_task(name):
    try:
        Task.insert(name=name).execute()
    except:
        BaseModel.conn.rollback()
        return


def add_progress(user_id, task_name):
    try:
        timestamp = datetime.datetime.utcnow().timestamp()
        Progress.create(user_id=user_id, task_name=task_name, timestamp=timestamp)
    except peewee.InternalError or peewee.IntegrityError:
        BaseModel.conn.rollback()
        return


def add_admin(tg_id):
    try:
        Admin.create(tg_id=tg_id)
    except:
        BaseModel.conn.rollback()
        return


def select_all_passed_task(user_id) -> typing.List[Progress]:
    try:
        usr_progress = Progress.select().where(Progress.user_id == user_id).execute()
        usr_progress = list(usr_progress)
        usr_progress.sort(key=lambda x: x.timestamp)
        return usr_progress
    except:
        BaseModel.conn.rollback()
        return []


def scoreboard(task_manager: TaskWorker) -> list:
    res = []
    users: typing.List[User] = User.select().execute()
    for usr in users:
        if if_in_admin_list(usr.tg_id):
            continue
        usr_progress = select_all_passed_task(usr.tg_id)
        score = 0
        for p in usr_progress:
            try:
                check = task_manager.tasks[p.task_id].chart['scorable']
                if check:
                    score += task_manager.tasks[p.task_id].chart['score']
            except KeyError:
                continue
        try:
            res.append([usr.username, score, usr_progress[-1].timestamp])
        except IndexError:
            res.append([usr.username, score, float('inf')])
    res = sorted(res, key=cmp_to_key(__cmp), reverse=True)
    return res


def get_user_score(tg_id, task_manager: TaskWorker):
    usr = User.get_by_id(tg_id)
    usr_progress = select_all_passed_task(usr.tg_id)
    score = 0
    for p in usr_progress:
        try:
            check = task_manager.tasks[p.task_id].chart['scorable']
            if check:
                score += task_manager.tasks[p.task_id].chart['score']
        except KeyError:
            continue
    return score


def __cmp(x: list, y: list):
    if x[1] > y[1]:
        return 1
    elif x[1] < y[1]:
        return -1
    else:
        if x[2] > y[2]:
            return -1
        elif x[2] < y[2]:
            return 1
        else:
            return 0


def select_unpassed_tasks(tg_id, task_manager: TaskWorker) -> typing.List[InternalTask]:
    try:
        usr_tasks = select_all_passed_task(tg_id)
        all_tasks = task_manager.tasks.copy()

        for p in usr_tasks:
            if p.task_id in all_tasks:
                all_tasks.pop(p.task_id)

        return all_tasks
    except:
        BaseModel.conn.rollback()
        return []


def get_user(tg_id):
    try:
        usr: User = User.get(tg_id)
        return usr
    except:
        BaseModel.conn.rollback()
        return None


def get_tg_by_username(username):
    usr = User.select().where(User.username == username).execute()
    for u in usr:
        return u.tg_id


def is_flag_passed(tg_id, flag, task_manager: TaskWorker):
    try:
        task_name = task_manager.get_task_by_flag(flag)
        res: Progress = Progress.get_by_id((tg_id, task_name))
        ts = res.timestamp
        return True
    except:
        return False


def get_users_tg_list():
    users = User.select().execute()
    res = []
    for user in users:
        res.append(user.tg_id)
    return res


def if_in_admin_list(tg_id):
    admins = Admin.select().execute()
    res = []
    for a in admins:
        res.append(a.tg_id_id)
    return tg_id in res


def delete_task(task_name):
    tasks = Task.select().where(Task.name == task_name).execute()
    for task in tasks:
        task.delete_instance()
