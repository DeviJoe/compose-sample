create table tasks
(
	id serial,
	name text not null
);

create unique index tasks_id_uindex
	on tasks (id);

create unique index tasks_name_uindex
	on tasks (name);

alter table tasks
	add constraint tasks_pk
		primary key (id);