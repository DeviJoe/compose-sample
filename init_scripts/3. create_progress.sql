create table progress
(
	user_id integer not null,
	task_id text not null,
	timestamp double precision not null,
	constraint progress_pk
		primary key (user_id, task_id)
);