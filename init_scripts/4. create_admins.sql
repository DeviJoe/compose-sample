create table admins
(
	tg_id integer not null
);

create unique index admins_tg_id_uindex
	on admins (tg_id);

alter table admins
	add constraint admins_pk
		primary key (tg_id);
