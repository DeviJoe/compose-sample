create table users
(
	tg_id integer,
	fio text not null,
	username text not null,
	vk_url text not null
);

create unique index users_tg_id_uindex
	on users (tg_id);

create unique index users_username_uindex
	on users (username);

create unique index users_vk_url_uindex
	on users (vk_url);

alter table users
	add constraint users_pk
		primary key (tg_id);


